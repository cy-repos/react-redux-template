import {combineReducers} from "redux";

const voidReducer = (state={message:"Hello world!"}, action) => state;

export default combineReducers({
    temp: voidReducer
});