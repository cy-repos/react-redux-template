import {createStore, applyMiddleware} from "redux";
import {composeWithDevTools} from 'redux-devtools-extension';
import logger from "redux-logger";

import reducers from "./reducers";

export default () => {
    const middleWare = applyMiddleware(logger());
    const store = createStore(reducers, composeWithDevTools(middleWare));

    return store;
};

